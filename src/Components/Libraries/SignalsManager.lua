--[[
    SignalsManager  1.0.0
    Modules/Services/SignalsManager.lua
    Responsible for implementing and managing all LuaSignals


    Changelog:
    /t
]]
----  localize environment
local require = require
local _G = _G
----  localize library
local root = root
local component = component
local PBCore = PBCore
local newObject = PBCore.newObject
local newObjectClass = PBCore.newObjectClass
local objectData = objectData environment.objectData = nil
local lookupAny = objectData.lookupAny
local Enums = PBCore:depend("EnumsManager").Enums
local Utilities = PBCore:depend("Utilities")
local Scheduler = PBCore:depend("ThreadManager").Scheduler
local wait = Scheduler.wait
local delay = Scheduler.delay
local spawn = Scheduler.spawnS
local coroutine = coroutine
local resumeThread = coroutine.resume
local yieldRunningThread = coroutine.yield
local getThreadStatus = coroutine.status
local createThread = coroutine.create
local getRunningThread = coroutine.running
local require = require
local setmetatable = setmetatable
local getmetatable = getmetatable
local os = os
local getTime = os.time
local table = table
local removeEntry = table.remove
local merge = Utilities.merge
local assign = Utilities.assign
local component = component
local __private_Component = lookupAny[component]
------  MODULE API
----  variables
local lookupSignalByConnection = setmetatable(
  {},
  {
    __mode = "kv"
  }
)
local totalSignals = setmetatable(
  {},
  {
    __mode = "kv"
  }
)  root.totalSignals = totalSignals
----  class definitions
--  @classdef  LuaSignalConnection
--  @desc  Represents the Listener Connection on a LuaSignal
--  @permissions:
--  read:  0 (LIBRARY)
--  write:  4 (NONWRITABLE)
--  execute: 0 (LIBRARY)
local classdefLuaSignalConnection = newObjectClass(
  "LuaSignalConnection", 
  lookupAny.Object.object
) do
  local __private_self = lookupAny.LuaSignalConnection
  ----  OnCreate
  function __private_self:onCreate(callback, LuaSignal)
    lookupAny.Object.onCreate(self)
    local __private_self = lookupAny[self]
    local __private_LuaSignal = lookupAny[LuaSignal]
    local attachedConnections = __private_LuaSignal.attachedConnections
    __private_self.signalCallback = callback
    __private_self.isConnected = true
    __private_self.LuaSignal = LuaSignal
    __private_self.objectRoot.connected = __private_self.isConnected
    lookupSignalByConnection[self] = LuaSignal
    attachedConnections[#attachedConnections + 1] = self
  end
  ----  API
  --  @param  none
  --  @desc  Returns the callback for the connection
  --  @return  function
  function classdefLuaSignalConnection:getCallback()
    return lookupAny[self].signalCallback
  end
  --  @param  none
  --  @desc  Disconnects the connection from the Listener
  function classdefLuaSignalConnection:disconnect()
    local __private_self = lookupAny[self]
    local LuaSignal = __private_self.LuaSignal
    __private_self.isConnected = false
    __private_self.objectRoot.connected = false
    local attachedConnections = __private_self.attachedConnections
    for entry = 1, #attached do
      if attached[i] == self then
        removeEntry(
          attachedConnections,
          entry
        )
        remove(attached, i)
        break
      end
    end
    lookupSignalByConnection[self] = nil
  end
  --  @param  none
  --  @desc  Disconnects then destroys the Object
  --  @returns  none
  local defaultDestroy = lookupAny.Object.destroy
  function classdefLuaSignalConnection:destroy()
    self:disconnect()
    defaultDestroy(self)
  end
end
--  @classdef  LuaSignal
--  @desc  Represents a custom LuaSignal.
--  @permissions
--  read:     0 (LIBRARY)
--  write:    4 (NONWRITABLE)
--  execute:  0 (LIBRARY)
local classdefLuaSignal = newObjectClass(
  "LuaSignal", 
  lookupAny.Object.object
) do
  local __self_LuaSignal = lookupAny.LuaSignal
  ----  variables
  local managedSignals = setmetatable(
    {},
    {
      __mode = "v"
    }
  )  __private_Component.managedSignals = managedSignals
  ----  OnCreate
  function __self_LuaSignal:onCreate()
    lookupAny.Object.onCreate(self)
    local __private_self = Construct.__private.loadedObjects[self]
    __private_self.attachedConnections = setmetatable(
      {},
      {
        __mode = "v"
      }
    )
    __private_self.waitingThreads = setmetatable(
      {},
      {
        __mode = "v"
      }
    )
    __private_self.objectRoot.Signal = Construct.new(
      "Signal", 
      nil, 
      self
    )
  end
  ----  LuaSignal API
  --  @param  vararg ...
  --  @desc  Fires the vararg to all waiting signals and connected listeners
  --  @returns  none
  function classdefLuaSignal:fire(...)
    local __private_self = lookupAny[self]
    local waitingThreads = __private_self.waitingThreads
    local attachedConnections = __private_self.attachedConnections
    --  resume suspended threads
    for i = 1, #__private_self.waitingThreads do
      local ran, failReason = resumeThread(
        waitingThreads[i],
        ...
      )
      if (ran == false) then
        error(
          Enums.Failure.LuaSignalThreadResumeFailure:format(
            tostring(waitingThreads[i]),
            self.name,
            failReason
          )
        )
      end
    end
    --  queue up listeners to run immediately after
    for i = 1, #attachedConnections do
      spawn(
        attachedConnections[i]:getCallback(),
        ...
      )
    end
  end
  --  @def  Signal
  --  @desc  Represents the Signal
  local classdefSignal = newObjectClass(
    "Signal", 
    lookupAny.Object.class
) do
    local __private_Signal = lookupAny.Signal.class
    ----  OnCreate
    function classdefSignal:onCreate(LuaSignal)
      lookupAny.Object.onCreate(self)
      lookupAny[self].LuaSignal = LuaSignal
    end
    ----  API
    --  @param  function callback
    --  @desc  Connects the callback to the LuaSignal, firing when :Fire is called
    --  @returns  LuaSignalConnection
    function classdefSignal:connect(callback)
      local __private_self = lookupAny[self]
      return newObject(
        "LuaSignalConnection", 
        nil, 
        callback, 
        __private_self.LuaSignal
      )
    end
    --  @param  vararg ...
    --  @desc  Adds the running thread to its waitingThreads, then yields the running thread
    --  @returns vararg
    function classdefSignal:wait()
      local __private_self = lookupAny[self]
      local waitingThreads = __private_self.waitingThreads
      local runningThread = getRunningThread()
      waitingThreads[#waitingThreads + 1] = runningThread
      return yieldRunningThread()
    end
  end
end
ready()