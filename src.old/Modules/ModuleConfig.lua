
local ModuleConfig = {}
------      INITIALIZATION
----     Localization, Variables, and Constants
--    Environment localization
local PigBot = PigBot
--    library localization
local UserData = PigBot.UserData
local LuaSignal = PigBot.LuaSignal
local BotConfig = PigBot.BotConfig
------      APIs
----    table  ModuleConfig:get()
----       Using PigBot:getModuleByThread(), will return the configuration table
----       for the requesting Module
function ModuleConfig:get()
  local module = PigBot:getModuleByThread()
  if module == nil or module == false then
    return error("not supported")
  end
  local data = UserData:get(
    PigBot.Client.user.id,
    module.name
  )
  if data == nil then
    --  saved data doesnt exist, use default data
    data = BotConfig[module.name]
  end
  return data
end
------  FINALIZATION
PigBot.ModuleConfig = ModuleConfig
ready()
