--  Modules/DiscordExtensions.lua
------  localization
--  the managed guild
local GUILD = Client:getGuild(BotConfig.Management.ManagedGuild)
--  channels usable by PigBot
local USABLE_CHANNELS = {
  ALERTS = GUILD:getChannel(BotConfig.Notifications.AlertsChannel),
  APPROVALS = GUILD:getChannel(BotConfig.Joining.ApprovalsChannel),
  INPUT = GUILD:getChannel(BotConfig.Management.InputChannel)
}
--  events
local SIGNALS = {
  messageCreate = LuaSignal.new(),
  memberPrescenceUpdate = LuaSignal.new(),
  memberLeave = LuaSignal.new(),
  memberJoin = LuaSignal.new(),
  reactionAdd = LuaSignal.new(),
  reactionRemove = LuaSignal.new(),
}
local EVENTS = {}
for name, signal in next, SIGNALS do
  EVENTS[name] = signal.Event
end
--
--------  MODULE API
declare__GUILD = GUILD
declare__USABLE_CHANNELS = USABLE_CHANNELS
declare__EVENTS = EVENTS
------  DiscordExtensions FUNCTIONS
----  Discord_Message  DiscordExtensions:sendNotification(string content)
----    Sends a message to the USABLE_CHANNELS.ALERTS.
function declare__sendNotification(self, content)
  Debugging.checkSelf(
    self,
    root
  )
  Debugging.checkType(
    content,
    "string",
    1
  )
  --  send the notification to USABLE_CHANNELS.ALERTS
  return USABLE_CHANNELS.ALERTS:send(content)
end
----  Discord_Message  DiscordExtensions:sendNotificationToMember(Discord_Member member, string content, boolean isImportant)
----    Sends a message to the member, if the member's USER_PREFERENCES allows it. If USER_PREFERENCES allow it,
----    the bot will also prefer to send DMs over channel messages. USER_PREFERENCES can also ignore notifications
----    which do not have the isImportant boolean set to true
function declare__sendNotificationToMember(self, member, content, isImportant)
  Debugging.checkSelf(
    self,
    root
  )
  Debugging.checkType(
    member,
    "table",
    1
  )
  Debugging.checkType(
    content,
    {
      "string",
      "table"
    },
    2
  )
  Debugging.checkType(
    isImportant,
    {
      "boolean",
      "nil"
    },
    3
  )
  --  see if were passed a member or user
  if member.user ~= nil then
    member = member.user
  end
  --  variables
  local preferDMs = UserData:get(
    member.id,
    "preferDMs"
  )
  local importantNotificationsOnly = UserData:get(
    member.id,
    "importantNotificationsOnly"
  )
  if preferDMs == nil then
    --  user doesnt have preferDMs set, default to system config.
    preferDMs = BotConfig.Notifications.PreferDMs
  end
  if importantNotificationsOnly == nil then
    -- default to false
    importantNotificationsOnly = false
  end
  Debugging.write(
    "preparing to send notification to %s\npreferDMs: %s\nimportantNotificationsOnly: %s",
    member.username,
    preferDMs,
    importantNotificationsOnly
  )
  ----  send the notification
  --  first, check to see if this is an important notification.
  if importantNotificationOnly == true and isImportant == false then
    --  user does not want non-important notifications, ignoring
    Debugging.write(
      "preferences for %s prevent us from sending non-important notifications",
      member.username
    )
    return false, "user preferences prevent us from sending non-important notifications"
  end
  --  second, lets see if we prefer DMs or channel alerts
  if preferDMs == true then
    --  try to send a DM first, falling back to AlertsChannel.
    local sendSuccess = member:send(content)
    --  if send was successful, return result
    if sendSuccess ~= nil then
      return sendSuccess
    else
      Debugging.write(
        "failed to send a direct message to %s, using notifications channel",
        member.username
      )
    end
  end
  --  if we've gotten here, the DM attempt failed. Notify them in the
  --  AlertsChannel
  return DiscordExtensions:sendNotification("<@".. member.id ..">\n".. content)
end
----  boolean  DiscordExtensions:isMemberStillInGuild(member)
----    Returns whether the member is still in the guild or not
local membersInGuild = {}
function declare__isMemberStillInGuild(self, memberId)
  Debugging.checkSelf(
    self,
    root
  )
  Debugging.checkType(
    memberId,
    {
      "string",
      "number"
    },
    1
  )
  ----  logic
  if membersInGuild[memberId] == true then
    return true
  end
  --  if we got here, then the member left.
  return false
end
----  boolean, string or DiscordMessage  DiscordExtensions:getReplyFromMember(member, timeoutCounter)
----    Waits until the member replies with anything, or timeoutCounter returns false
function declare__getReplyFromMember(self, member, timeoutCounter)
  Debugging.checkSelf(
    self,
    root
  )
  local __member_Type = type(member)
  local __timeoutCounter_type = type(timeoutCounter)
  Debugging.assert(
    __member_type ~= "nil",
    "bad argument #1 (member cannot be nil)"
  )
  Debugging.assert(
    __timeoutCounter_type ~= "nil",
    "bad argument #2 (timeoutCounter cannot be nil)"
  )
  ----  variables
  local userReply
  ----  logic
  --  since we want to be able to "time out", we have to use a loop and wait for the response.
  spawn(
    function()
      --  have to wait on userReply so we can check to see if its our member
      --  whose replying
      while userReply == nil do
        --  first, lets see if the member is still with us.
        if DiscordExtensions:isMemberStillInGuild(member.id) == false then
          --  user left on us, time to walk out.
          return false, "user left server"
        end
        --  wait for the message
        userReply = EVENTS.messageCreate:wait()
        --  userReply received, it is from our member?
        if userReply.author.id ~= member.id then
          --  nope, set userReply to nil and try again
          userReply = nil
        end
      end
    end
  )
  --  member and time out checker
  while userReply == nil do
    --  first, lets see if the member is still with us.
    if DiscordExtensions:isMemberStillInGuild(member.id) == false then
      --  user left on us, time to walk out.
      return false, "user left server"
    end
    --  second, lets see how long weve been at this.
    if timeoutCounter() == false then
      --  weve ran out of usable time, boogie out.
      return false, "user timed out"
    end
    --  wait for 1 second, try again
    wait(1)
  end
  --  return our reply
  return true, userReply
end
----  boolean, string or DiscordMessage  DiscordExtensions:getSpecificReplyFromMember(member, timeoutCounter, string or table expectedReply)
----    Waits until the member replies with a sppecific response, otherwise will continue to listen until the
----    member does so
function declare__getSpecificReplyFromMember(self, member, timeoutCounter, expectedReply)
  Debugging.checkSelf(
    self,
    root
  )
  Debugging.checkType(
    timeoutCounter,
    "function",
      2
  )
  Debugging.checkType(
    expectedReply,
    {
      "string",
      "table"
    },
    3
  )
  local __expectedReply_Type = type(expectedReply)
  if __expectedReply_Type == "string" then
    expectedReply = {expectedReply}
  end
  local expectedReplyLookup = {}
  for i = 1, #expectedReply do
    expectedReplyLookup[expectedReply[i]] = true
  end
  ----  variables
  local success, response, checkingResponse
  ----  logic
  --  begin waiting for a response
  while success == nil do
    --  get our reply
    success, response = DiscordExtensions:getReplyFromMember(
      member,
      timeoutCounter
    )
    --  if we didnt succeed, go ahead and exit now.
    if success == false then
      return success, response
    end
    --  otherwise, check to see if its the response were expecting
    checkingResponse = expectedReplyLookup[response.content]
    --  check to see if its a expected reply
    if checkingResponse ~= nil then
      --  we got our response, boogy out of here
      return true, response.content
    else
      --  we got an unexpected response, lets try again
      local expectedResponses = "**Sorry, I don't speak gibberish**\n\nPlease reply with "
      for i = 1, #expectedReply do
        expectedResponses = expectedResponses .."\"".. expectedReply[i] .."\" or"
      end
      expectedResponses = expectedResponses:sub(0, #expectedResponses - 3)
      --  send the notification to our member
      DiscordExtensions:sendNotificationToMember(
        member,
        expectedResponses,
        true
      )
      success = nil
    end
    --  lets wait for 1 second, then try again
    wait(1)
  end
end
----  boolean, string or DiscordReaction  DiscordExtensions:waitForReaction(DiscordMessage message, Emoji emoji)
----    Waits until the emoji is available on message, then returns it
function declare__waitForReaction(self, message, emoji)
  Debugging.checkSelf(
    self,
    root
  )
  Debugging.checkType(
    emoji,
    "string",
    2
  )
  local __message_Type = type(message)
  Debugging.assert(__message_Type ~= "nil", "bad argument #1 (message cannot be nil)")
  ----  variables
  local DiscordReaction
  ----  logic
  while DiscordReaction == nil do
    DiscordReaction = message.reactions[2][emoji]
    if DiscordReaction == nil then
      wait(1)
    end
  end
  return DiscordReaction
end
------  DiscordExtensions EVENT CONNECTIONS
------  connects our Signals to Discordia API
----  process inputs from either DMs or our dedicated USABLE_CHANNELS.INPUT channel. ignore everything
----  else.
Client:on(
  "messageCreate",
  function(newMessage)
    --  first, ensure we ignore messages from ourself
    if newMessage.author.id == Client.user.id then
      return
    end
    --  second, check to see if this message came from either our USABLE_CHANNELS.InputChannel or a User DMs
    local isGuild = newMessage.guild
    if isGuild then
      isGuild = GUILD.id == newMessage.guild.id
    else
      isGuild = false
    end
    if isGuild then
      local isInterface = newMessage.channel.id == USABLE_CHANNELS.INPUT.id
      if isInterface then
        SIGNALS.messageCreate:fire(newMessage, isGuild)
      end
    else
      if newMessage.guild == nil then
        SIGNALS.messageCreate:fire(newMessage, isGuild)
      end
    end
  end
)
----  when a member leaves, fire our MEMBER_LEFT_SIGNAL
Client:on(
  "memberLeave",
  function(leavingMember)
    --  check to see if leavingMember has our basic membership role
    if leavingMember:hasRole(BotConfig.Joining.RoleToAssign) then
      --  member is leaving the server, lets save their data
      local leftData = {
        time = os.time(),  --  when did leavingMember leave? very important to know
        roles = {}
      }
      --  store all of the roles they had
      for role in leavingMember.roles:iter() do
        leftData.roles[#leftData.roles + 1] = role.id
      end
      --  actually save data to UserData
      UserData:set(
        leavingMember.id,
        "leftData",
        leftData
      )
    end
    --  fire MEMBER_LEFT_SIGNAL
    membersInGuild[leavingMember.id] = nil
    SIGNALS.memberLeave:fire(leavingMember)
  end
)
----  fire when a member joins
Client:on(
  "memberJoin",
  function(joiningMember)
    membersInGuild[joiningMember.id] = true
    SIGNALS.memberJoin:fire(joiningMember)
  end
)
----  fire when a new reaction is added
Client:on(
  "reactionAdd",
  function(reaction, userId)
    SIGNALS.reactionAdd:fire(reaction, userId)
  end
)
----  fire when a reaction is removed
Client:on(
  "reactionRemove",
  function(reaction, userId)
    SIGNALS.reactionRemove:fire(reaction, userId)
  end
)
------  FINALIZATION
ready()
--  populate our table so we dont mess with the Discordia cache
PigBot.pbReady:wait()
for _, member in next, GUILD.members[2] do
  membersInGuild[member.id] = true
end
