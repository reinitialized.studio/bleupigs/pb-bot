--  Modules/Debugging.lua
------  MODULE INITIALIZATIOn
------  MODULE API
local d_errorCodes = {
  "This function expects to be called using \":\" or having it's root table passed as its first argument",
  "bad argument #%d (expected type %s, got %s)",
  "bad variable (expected type %s, got %s)"
}
----
-- API BEGIN
----
--  void  Debugging.checkSelf(table table1, table table2)
--    Requires table1 to match table2, otherwise will error
function declare__checkSelf(table1, table2)
  if BotConfig.Debugging.Enabled then
    Debugging.checkType(
      table1,
      "table",
      1
    )
    Debugging.checkType(
      table2,
      "table",
      2
    )
    ----
    if table1 ~= table2 then
      Debugging.terminate(d_errorCodes[1])
    end
  end
end
----
--  void  Debugging.write(string contents, ...)
--    Writes a debug message, if BotConfig allows it
local debugMessageOut = "[DEBUG %s - %s@%s]\n%s\n\n%s"
function declare__write(contents, ...)
  if BotConfig.Debugging.Enabled == true then
    --  get our name
    local name = getfenv(2).module
    if name ~= nil then
      name = name.name
    else
      name = "GLOBAL"
    end
    --  check to see if module is enabled
    if BotConfig.Debugging.FilterModules == true and BotConfig.Debugging.Modules[name] ~= true then
      return
    end
    if BotConfig.Debugging.FilterFunctions == true and BotConfig.Debugging.Functions[debug.getinfo(2).name] ~= true then
      return
    end
    --  create the entry
    local entry = Logging.write(
      0,
      contents,
      ...
    )
    ----  send information to output
    print(
      debugMessageOut:format(
        name,
        entry[5],
        entry[4],
        entry[1],
        entry[3]
      )
    )
  end
end
----
--  void  Debugging.checkType(checking, string or table expectedType, number argument)
--    Checks to see if checking matches any of expectedType, or will error
function declare__checkType(checking, expectedType, argument)
  if BotConfig.Debugging.Enabled then
    local __type_argument = type(argument)
    local __type_expectedType = type(expectedType)
    if __type_expectedType == "string" then
      expectedType = {expectedType}
      __type_expectedType = "table"
    end
    if __type_expectedType ~= "table" then
      Logging.write(
        4,
        d_errorCodes[2]:format(
          2,
          "string or table",
          __type_expectedType
        )
      )
      error("PigBot has experienced an fatal error.")
    end
    if __type_argument ~= "nil" then
      if __type_argument ~= "number" then
        Logging.write(
          4,
          d_errorCodes[2]:format(
            3,
            "number",
            __type_argument
          )
        )
        error("PigBot has experienced an fatal error.")
      end
    end
    --
    local checkingType = type(checking)
    for number, expectingType in next, expectedType do
      if (checkingType == expectingType) then
        return
      end
    end
    local response = expectedType[1]
    if (#expectedType > 1) then
      for key = 2, #expectedType do
        response = expectedType[key]
        if (key ~= #expectedType) then
          response = response .." or  "
        end
      end
    end
    ----
    if argument ~= nil then
      return Debugging.terminate(
        d_errorCodes[2],
        argument,
        response,
        checkingType
      )
    else
      return Debugging.terminate(
        d_errorCodes[3],
        response,
        checkingType
      )
    end
  end
end
----
--  void  Debugging.terminate(string message, ...)
--    Records the error, then throws an exception
function declare__terminate(message, ...)
  Debugging.checkType(
    message,
    "string",
    1
  )
  ----
  local entry = Logging.write(
    4,
    message,
    ...
  )
  return error("PigBot has experienced an fatal error.")
end
----
--  void  Debugging.assert(condition, string message, ...)
--    If condition does not equal true, will throw an exception.If
function declare__assert(condition, message, ...)
  Debugging.checkType(
    message,
    {
      "string",
      "nil"
    },
    2
  )
  if message == nil then
    message = "assert failed"
  end
  ----
  if condition ~= true then
    return Debugging.terminate(
      message,
      ...
    )
  end
end

do -- SOURCED VIA https://github.com/RoStrap/Debug/blob/master/Debug.module.lua
	--- Converts a table into a readable string
	-- string TableToString(table Table, string TableName)
	-- @param table Table The Table to convert into a readable string
	-- @param string TableName Optional Name parameter that puts a "[TableName] = " at the beginning
	-- @param AlphabeticallyArranged Whether the table should be alphabetically sorted: still in-dev, little support
	-- @returns a readable string version of the table

	local function Parse(Object, EncounteredTables)
		local Type = type(Object)

		if Type == "table" then
			for TableName, Table in next, EncounteredTables do
				if Table == Object then
					if TableName == 1 then
						return "[self]"
					else
						return "[table " .. TableName .. "]"
					end
				end
			end
			return root.TableToString(Object, nil EncounteredTables)
		end

		return
			Type == "string" and "\"" .. Object .. "\"" or
			(Type == "function" or Type == "userdata") and Type or
			tostring(Object)
	end

	function declare__TableToString(Table, TableName, EncounteredTables)
		if type(Table) == "table" then
			local EncounteredTables = EncounteredTables or {}
			EncounteredTables[TableName or #EncounteredTables + 1] = Table
			local IsArrayKey = {}
			local Output = {}
			local OutputCount = 0

			for Integer, Value in ipairs(Table) do
				IsArrayKey[Integer] = true
				Output[OutputCount + 1] = Parse(Value, EncounteredTables)
				Output[OutputCount + 2] = ", "
				OutputCount = OutputCount + 2
			end

			for Key, Value in next, Table do
				if not IsArrayKey[Key] then
					if type(Key) == "string" and not Key:find("^%d") then
						Output[OutputCount + 1] = Key
						OutputCount = OutputCount - 2
					else
						Output[OutputCount + 1] = "["
						Output[OutputCount + 2] = Parse(Key, AlphabeticallyArranged, EncounteredTables)
						Output[OutputCount + 3] = "]"
					end

					Output[OutputCount + 4] = " = "
					Output[OutputCount + 5] = Parse(Value, AlphabeticallyArranged, EncounteredTables)
					Output[OutputCount + 6] = ", "
					OutputCount = OutputCount + 6
				end
			end

			Output[OutputCount] = nil

			local Metatable = getmetatable(Table)

			Output = "{" .. table.concat(Output) .. "}"

			if Metatable then
				if type(Metatable) == "table" then
					Output = Output .. " <- " .. TableToString(Metatable)
				else
					Logging.write(
            0,
            (TableName or "Table") .. "'s metatable cannot be accessed. Got:\n" .. tostring(Metatable)
          )
				end				
			end

			if TableName then
				Output = TableName .. " = " .. Output
			end

			return Output
		else
			root.write("TableToString needs a table to convert to a string! Got type" .. typeof(Table), 2)
		end
	end
end
------  FINIALIZE
ready()
