--[[
    PigBot BattleTester
    version 31618 experimental

    The BattleTester is a lightweight testing bot built specifically to test the Bleu Pigs
    PigBot Management system.

    
    Changelog:
        31618:
            - BattleTester implementation begin
            https://gitlab.com/bleu-pigs/pig-bot/issues/8

        31818:
            - 
]]
-----       PRE-INITIALIZATION
----    localize and predefine
local API = PigBot
local Discordia = require("discordia")
local btClient = Discordia.Client(
    {
        cacheAllMembers = true,
        syncGuilds = true
    }
)
local fs = require("filesystem")
local LuaSignal = API.LuaSignal
local testModulesPath = "src/tests/modules"
local testModules = fs.scandir(testModulesPath)
--  setup the Ready listener